<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Events\SendChat;
use App\User;
use App\Chat;
use Pusher;
use App\ChatNotSend;
use Auth;


class SendChatController extends Controller
{

    public $pusher;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     */

     public function __construct()
     {
        $this->pusher = new Pusher\Pusher;
     }

    public function index()
    {
        // dd();
        $user = User::all();
        $userOnline = $this->pusher->get_channels();
        $userOnline =$userOnline->channels;
        $userOnline = array_keys($userOnline);
        // dd($userOnline);
        return view('send-chat.index', compact(['user', 'userOnline']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function unsetData($data)
    {
        unset($data['user']);
        unset($data['user_chated']);
        
        return $data;
    }

    public function store(Request $r)
    {
        $data = [
            'from_user_id' => $r['user_id'],
            'to_user_id' => $r['to_user_id'],
            'user' => $r['user'],
            'user_chated' => $r['user_chated'],
            'message' => $r['message'],
            'date' => date('m-d-Y H:i'),
        ];

        $dataBackup = $data;
        $checkOnline = $this->pusher->get_channel_info($r['user_chated']);
        $dataOffline = $data;
        $dataOffline = $this->unsetData($dataOffline);

        

        if($checkOnline->occupied)
        {
            $pusher = event(new SendChat($data));
            $chatDB = Chat::create($dataOffline);
        }
        else
        {
            $chatDB = ChatNotSend::create($dataOffline);
        }


        $name_cookie = $r['user_id'] . '&' . $r['to_user_id'];
        $minutes = time() + (86400 * 30); // sebulan
        $data = $this->unsetData($data);
        $this->addCookie($name_cookie, $minutes, $data);
        unset($dataOffline['from_user_id']);
        unset($dataOffline['to_user_id']);
        return $dataOffline;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function receiveChat(Request $r)
    {
        $data = (array)$r['data'];

        $dataBackup = $data;
        
        $name_cookie =  $r['data']['to_user_id'] . '&' . $r['data']['from_user_id'];
        $minutes = time() + (86400 * 30);

        return $this->addCookie($name_cookie, $minutes, $data);

        
    }

    public function addCookie($name_cookie, $minutes, $data)
    {
        for ($i=1; $i <= 1; $i++) 
        {
            if ( isset($_COOKIE[$name_cookie.$i]) ) 
            {
                $chat = json_decode($_COOKIE[$name_cookie.$i], true);
                if( (strlen(serialize($_COOKIE[$name_cookie.$i])) * 2) <= 4500 )
                {
                    $data = $this->unsetData($data);
                    $chat["chat".(1+count($chat))] = $data;
                    setcookie($name_cookie.$i, json_encode($chat), $minutes);
                    // $data = $this->unsetData($data);
                    // $chatDB = Chat::create($data);

                    return $data['message'];
                }
            }
            else
            {
                $data = $this->unsetData($data);
                $chat = [
                    "chat1" => $data,
                ];
                setcookie($name_cookie.$i, json_encode($chat), $minutes);
                // $data = $this->unsetData($data);
                // $chatDB = Chat::create($data);

                return $data['message'];
            }
        }
    }

    public function chatNotSend(Request $request)
    {
        // return $request;
        // $chatNotSend;
        if(isset($request['id']))
        {
            $chatNotSend =  ChatNotSend::where('to_user_id', $request['id'])->get();
        }
        else
        {
            $chatNotSend =  ChatNotSend::where('to_user_id', Auth::user()->id)->get();
        }

        return $chatNotSend;
    }

    public function moveChat(Request $r)
    {
        // dd(gettype($r['to_user_id']));
        $sendChat = Chat::create([
            "from_user_id" => $r['from_user_id'],
            "to_user_id" => $r['to_user_id'],
            "message" => $r['message'],
            "date" => $r['date'],
        ]);

        $name_cookie = $r['to_user_id'] . '&' . $r['from_user_id'];
        $minutes = time() + (86400 * 30);
        

        $this->addCookie($name_cookie, $minutes, $sendChat);

        ChatNotSend::where('id', $r['id'])->delete();

        return $sendChat;
    }
    
}
