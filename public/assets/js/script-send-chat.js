$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    
    $('#action_menu_btn').click(function () {
        $('.action_menu').toggle();
    });

    replaceString = (data) => 
    {
        data = data.replace(/(\r\n|\n|\r)/g, '');
        data = data.replace(/ /g, '');

        return data;
    }

    let cookie_chat_not_read = [];
    let html, status_read;
    
    $(document).on("click",".contacts li", function (event) 
    {
        let name = $(event.target).children('.user_info').text();
        name = replaceString(name);
        let user = $('.user').val();
        let userid = $('#from_user_chat').val();
        let to_user_id = $('#chat_'+name).val();
        let status_user = $('#status_'+name).val();
        $.get("chat-not-send", {'id':to_user_id},function(data, status){
            data = Object.entries(data);
            data.forEach(d => {
                console.log();
                cookie_chat_not_read.push(d[1]['message'] + d[1]['date']);
            });
            html = `
            <div class="card-header msg_head">
                        <div class="d-flex bd-highlight">
                            <div class="img_cont">
                                <img src="https://res.cloudinary.com/duh6epdw5/image/upload/v1594839491/wns14nhbqe7hbzmqgoh0.png" alt="no image profil"
                                    class="rounded-circle user_img">
                                <span class="`+ status_user +`_icon"></span>
                            </div>
                            <div class="user_info">
                                <span>` + name + `</span>
                            </div>
                            <div class="video_cam">
                                <span><i class="fas fa-video"></i></span>
                                <span><i class="fas fa-phone"></i></span>
                            </div>
                        </div>
                        <span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
                        <div class="action_menu">
                            <ul>
                                <li><i class="fas fa-user-circle"></i> View profile</li>
                                <li><i class="fas fa-users"></i> Add to close friends</li>
                                <li><i class="fas fa-plus"></i> Add to group</li>
                                <li><i class="fas fa-ban"></i> Block</li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body msg_card_body">
                        `;
                        for (let i = 1; i < 1000; i++) 
                        {
                            let cookiename = userid + '&' + to_user_id + i;
                            let cookie = $.cookie(cookiename);
                            if( cookie != null)
                            {
                                cookie = JSON.parse(cookie);
                                let amountCookie = Object.keys(cookie).length;
                                // console.log(cookie);
                                for (let a = 1; a <= amountCookie; a++) 
                                {
                                    b = 0;
                                    console.log(cookie['chat'+a]);
                                    if( cookie_chat_not_read.includes(cookie['chat'+a]['message']+cookie['chat'+a]['date']) )
                                    {
                                        status_read = 'belum dibaca';
                                    }
                                    else
                                    {
                                        status_read = 'udah dibaca';
                                    }
                                    if(cookie['chat'+a]['from_user_id'] == userid)
                                    {
                                        html += `
                                        <div class="d-flex justify-content-end mb-4">
                                        
                                        <div class="msg_cotainer_send text-right" style="min-width: 25%;">
                                            ` + cookie['chat'+a]['message'] + `
                                            <span class="msg_time_send">` + status_read + ` `+  cookie['chat'+a]['date'] +`</span>
                                        </div>
                                        <div class="img_cont_msg">
                                            <img src="https://res.cloudinary.com/duh6epdw5/image/upload/v1594839491/wns14nhbqe7hbzmqgoh0.png" alt="no image"
                                                class="rounded-circle user_img_msg">
                                        </div></div>`;
                                    }
                                    else
                                    {
                                        html += `
                                        <div class="d-flex justify-content-start mb-4">
                                            <div class="img_cont_msg">
                                                <img src="https://res.cloudinary.com/duh6epdw5/image/upload/v1594610178/account_default1.jpg" alt="no image profil"
                                                    class="rounded-circle user_img_msg">
                                            </div>
                                            <div class="msg_cotainer" style="min-width: 25%;">
                                                `+ cookie['chat'+a]['message'] +`
                                                <span class="msg_time">`+ cookie['chat'+a]['date']+`</span>
                                            </div>
                                        </div>`;
                                    }
                                }
                            }
                        }
                        html += `
                    </div>
                    </div>
                    <div class="card-footer">
                        <div class="input-group">
                            <div class="input-group-append">
                                <span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span>
                            </div>
                            <input type="hidden" class="user-chated" value="`+name+`">
                            <textarea name="" class="form-control type_msg"
                                placeholder="Type your message..."></textarea>
                            <div class="input-group-append" id="send-message" onclick="sendChat('`+ name +`')">
                                <span class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></span>
                            </div>
                        </div>
                    </div>`;

                    $('.room-chat').html(html);
            });
        // return false;
            
        

        

    });

    // $('.contacts li').click( function() {
    //     console.log(data);
        
    //     $('.room-chat').append(html);
    // });


    
    // $('#send-message').click( () => {
    //     console.log('jjjjjjj');
    //     let message = $('.type_msg').val();
    //     console.log(message);
        
        
    // });

    sendChat = (name) => {

        let message = $('.type_msg').val(); 
        let user = $('.user').val();
        let user_chated = $('.user-chated').val();
            user_chated = replaceString(user_chated);
        let user_id = $('#from_user_chat').val();
        let to_user_id = $('#chat_'+name).val();
        
        
        $.post('/send-chat', { 'user':user, 'user_chated':user_chated, 'message':message, 'user_id':user_id, 'to_user_id':to_user_id }, function(data){
            let tgl = new Date('08-01-2020 10:27');
                console.log(tgl);
                // tgl = timeSince(tgl);
                console.log(tgl);
                
            let chat = `
                <div class="d-flex justify-content-end mb-4">
                    <div class="msg_cotainer_send text-right" style="min-width: 12%;">
                        ` + data['message'] + `
                        <span class="msg_time_send">`+ tgl + `</span>
                    </div>
                    <div class="img_cont_msg">
                        <img src="https://res.cloudinary.com/duh6epdw5/image/upload/v1594839491/wns14nhbqe7hbzmqgoh0.png" alt="no image"
                            class="rounded-circle user_img_msg">
                    </div>
                </div>`;
            $('.msg_card_body').append(chat);   
        }).fail(function() {
        alert( "error" );
        });
    }

    

    // var aDay = 24*60*60*1000;
    // console.log(aDay);
    // console.log(new Date(Date.now()-aDay));
    
    


});